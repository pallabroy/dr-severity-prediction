# README #

Step 1:

Clone your code directory from the git



Step2:

Cd into git repo

Run this command

floyd init [repo-name]



Step 3:  load and sync data

Cd into data directory  and run this command

Floyd data init [data dir name]

Floyd data upload



Step 4: Run a code  to test prediction



floyd run --env keras --data pallab/datasets/dr-severity-data/2:input --gpu "python predict_dr.py"



Step 5: create app.py for serving the model



floyd run –env keras --data pallab/datasets/dr-severity-data/2:input  --gpu --mode serve