import glob
import os
import numpy as np
import pandas as pd
import cv2
r=256


def estimate_radius(img):
    mx = img[img.shape[0] // 2, :, :].sum(1)
    rx = (mx > mx.mean() / 10).sum() / 2

    my = img[:, img.shape[1] // 2, :].sum(1)
    ry = (my > my.mean() / 10).sum() / 2

    return (ry, rx)


def subtract_gaussian_blur(img):
    gb_img = cv2.GaussianBlur(img, (0, 0), 5)

    return cv2.addWeighted(img, 4, gb_img, -4, 128)


def remove_outer_circle(a, p, r):
    b = np.zeros(a.shape, dtype=np.uint8)
    cv2.circle(b, (a.shape[1] // 2, a.shape[0] // 2),
               int(r * p), (1, 1, 1), -1, 8, 0)

    return a * b + 128 * (1 - b)


def crop_img(img, h, w):
    h_margin = (img.shape[0] - h) // 2 if img.shape[0] > h else 0
    w_margin = (img.shape[1] - w) // 2 if img.shape[1] > w else 0

    crop_img = img[h_margin:h + h_margin, w_margin:w + w_margin, :]

    return crop_img


def place_in_square(img, r, h, w):
    new_img = np.zeros((2 * r, 2 * r, 3), dtype=np.uint8)
    new_img += 128
    new_img[r - h // 2:r - h // 2 + img.shape[0],
            r - w // 2:r - w // 2 + img.shape[1]] = img

    return new_img


def preprocess(img):
    try:
        ry, rx = estimate_radius(img)
        resize_scale = r / max(rx, ry)
        w = min(int(rx * resize_scale * 2), r * 2)
        h = min(int(ry * resize_scale * 2), r * 2)

        img = cv2.resize(img, (0, 0), fx=resize_scale, fy=resize_scale)

        img = crop_img(img, h, w)
        # print("crop_img", np.mean(img), np.std(img))

        img = subtract_gaussian_blur(img)
        img = remove_outer_circle(img, 0.9, r)
        img = place_in_square(img, r, h, w)

        return img

    except Exception as e:
        print("exception {}".format(e))

    return None



