import flask
from flask import Flask, request,redirect, url_for, make_response, jsonify
import pybase64
import numpy as np
from io import BytesIO
from models import get_model
from preprocess_img import preprocess
from matplotlib import pyplot as plt
import cv2
from predict_dr import DrPredictor
from PIL import Image

model_path='/input/models/cnn_v1.hdf5'
input_size=512
dr_predictor=DrPredictor(model_path,input_size)



app = Flask(__name__)


def do_dr_prediction(img):
    x = np.array(img)
    x=preprocess(x)
    #x = np.rollaxis(x, 2, 0)
    x = np.expand_dims(x, axis=0)
    severity=dr_predictor.do_prediction(x)
    if severity==0:
        pred='No-Diabetic Retinopathy'
    else:
        pred='Diabetic Retinopahty'
    return pred


@app.route('/<path:path>', methods=["POST"])
def predict_dr_severity(path):
    if 'img' in request.json:
        encoded_image = request.json['img']
        img = Image.open(BytesIO(pybase64.b64decode(encoded_image)))
    else:
        abort(BAD_REQUEST)
    pred = do_dr_prediction(img)
    print('Prediction {}'.format(pred))

    return make_response(jsonify({'Prediction': pred}), 200)


# Load the model and run the server
if __name__ == "__main__":
    app.run(host='0.0.0.0')
