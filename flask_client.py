import requests
import json
import pybase64
from PIL import Image
from io import BytesIO
from matplotlib import pyplot as plt
ENCODING = 'utf-8'

if __name__ == '__main__':
    img_path = '../dr-severity-data/test-img/58_left.jpeg'

    with open(img_path, "rb") as image_file:
        encoded_img = pybase64.b64encode(image_file.read())

    encoded_img = encoded_img.decode(ENCODING)

    headers = {'Content-type': 'application/json'}
    data = {'img': encoded_img}

    res = requests.post('https://www.floydlabs.com/expose/ZtcmrbCKC4VHzncKr62qPA',
                        data=json.dumps(data), headers=headers)

    print(res.text)
