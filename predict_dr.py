from models import get_model
from keras.utils.data_utils import Sequence
from PIL import Image
from preprocess_img import preprocess
from matplotlib import pyplot as plt
import cv2
import numpy as np



class DrPredictor(object):
    def __init__(self,model_path,input_size):
        self.model=get_model(input_size)
        self.model.load_weights(model_path)

    def do_prediction(self,img):
        preds = self.model.predict(img)
        severity = preds.argmax(axis=-1)[0]
        return severity




# img_path='/input/test-img/687_right.jpeg'
# model_path='/input/models/cnn_v1.hdf5'
# input_size=512
# img=np.array(Image.open(img_path))
# x=preprocess(img)
# #x = np.rollaxis(x, 2, 0)
# x = np.expand_dims(x, axis=0)
# print(x.shape)

# dr_predictor=DrPredictor(model_path,input_size)
# severity=dr_predictor.do_prediction(x)
# print(severity)




